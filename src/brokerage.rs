use {
    crate::{Bar, Microdollar, OptionsContract, OptionsStrategy, Period, Quote, StockSymbol},
    async_trait::async_trait,
    chrono::{DateTime, Datelike, Duration, Utc},
    futures::{
        channel::{mpsc, oneshot},
        future::FutureExt,
        sink::SinkExt,
        stream::StreamExt,
        Future,
    },
    serde::Serialize,
    std::{fmt::Debug, sync::Arc},
    thiserror::Error,
    tokio::sync::broadcast,
    tracing::info,
};

#[derive(Debug, Clone, PartialEq)]
pub enum StreamItem<OID: Debug + Clone + PartialEq> {
    MinuteBar(StockSymbol, Bar),
    Quote(StockSymbol, Quote),
    OptionsContract(StockSymbol, Vec<OptionsContract>),
    OrderState(OID, OrderState),
}

#[async_trait]
pub trait Stream {
    type Error: Send + Sync + 'static;
    type OrderId: Debug + Clone + PartialEq + Send + Sync + 'static;

    async fn subscribe_to_minute_bars(
        &mut self,
        underlyings: &[StockSymbol],
    ) -> Result<(), Self::Error>;

    async fn subscribe_to_quotes(&mut self, underlyings: &[StockSymbol])
        -> Result<(), Self::Error>;

    async fn subscribe_to_options_contracts(
        &mut self,
        underlyings: &[StockSymbol],
    ) -> Result<(), Self::Error>;

    async fn subscribe_to_order_states(&mut self) -> Result<(), Self::Error>;

    async fn next(&mut self) -> Result<Vec<StreamItem<Self::OrderId>>, Self::Error>;

    async fn shutdown(self) -> Result<(), Self::Error>;
}

pub struct SharedStream<B: Brokerage> {
    subscribe_to_order_state_requests:
        mpsc::Sender<oneshot::Sender<broadcast::Receiver<(B::OrderId, OrderState)>>>,
}

#[derive(Debug, Clone, Copy, PartialEq)]
pub enum OrderState {
    PreOpen,
    Open {
        quantity: u32,
    },
    Rejected,
    PendingCancel,
    Canceled {
        filled_quantity: u32,
        total_quantity: u32,
    },
    Filled {
        quantity: u32,
    },
    PartiallyFilled {
        filled_quantity: u32,
        total_quantity: u32,
    },
}

#[derive(Error, Debug)]
pub enum SharedStreamError {
    #[error("Failed to send order status subscription request: {0}")]
    FailedToSendOrderStateSubscriptionRequest(mpsc::SendError),
    #[error("Failed to get order status stream: {0}")]
    FailedToGetOrderStateStream(oneshot::Canceled),
}

#[derive(Error, Debug)]
pub enum RunError<BE, BSE> {
    #[error("Failed to get brokerage stream: {0}")]
    FailedToGetBrokerageStream(BE),
    #[error("Failed to subscribe to order status: {0}")]
    FailedToSubscribeToOrderState(BSE),
    #[error("Failed to get stream items: {0}")]
    FailedToGetStreamItems(BSE),
    #[error("Failed to shutdown stream: {0}")]
    FailedToShutdownStream(BSE),
}

impl<
        BE: Send + Sync + 'static,
        BSE: Send + Sync + 'static,
        OID: Debug + Clone + PartialEq + Send + Sync + 'static,
        BS: Stream<Error = BSE, OrderId = OID> + Send + Sync + 'static,
        B: Brokerage<Error = BE, OrderId = OID, Stream = BS> + Send + Sync + 'static,
    > SharedStream<B>
{
    async fn run(
        brokerage: &B,
        mut subscribe_to_order_state_requests: mpsc::Receiver<
            oneshot::Sender<broadcast::Receiver<(OID, OrderState)>>,
        >,
        check_subscribers_interval: Duration,
    ) -> Result<(), RunError<BE, BSE>> {
        loop {
            if !Self::run_until_no_subscribers(
                brokerage,
                &mut subscribe_to_order_state_requests,
                check_subscribers_interval,
            )
            .await?
            {
                return Ok(());
            }
        }
    }

    async fn run_until_no_subscribers(
        brokerage: &B,
        subscribe_to_order_state_requests: &mut mpsc::Receiver<
            oneshot::Sender<broadcast::Receiver<(OID, OrderState)>>,
        >,
        check_subscribers_interval: Duration,
    ) -> Result<bool, RunError<BE, BSE>> {
        async fn handle_stream_item<OID: Debug + Clone + PartialEq + Send + Sync + 'static>(
            order_state_tx: &mut broadcast::Sender<(OID, OrderState)>,
            stream_item: StreamItem<OID>,
        ) -> bool {
            let mut check_for_no_subscribers = false;
            match stream_item {
                StreamItem::OrderState(order_id, order_state) => {
                    if let Err(_) = order_state_tx.send((order_id, order_state)) {
                        check_for_no_subscribers = true;
                    }
                }
                _ => {}
            }
            check_for_no_subscribers
        }

        fn no_subscribers<OID>(order_state_tx: &broadcast::Sender<(OID, OrderState)>) -> bool {
            order_state_tx.receiver_count() == 0
        }

        // Get initial subscribers before starting the stream.
        let (mut order_state_tx, _) = broadcast::channel::<(OID, OrderState)>(1024);
        if let Some(dst) = subscribe_to_order_state_requests.next().await {
            let _ = dst.send(order_state_tx.subscribe());
        } else {
            return Ok(false);
        };

        let mut stream = brokerage
            .get_stream()
            .await
            .map_err(|err| RunError::FailedToGetBrokerageStream(err))?;
        stream
            .subscribe_to_order_states()
            .await
            .map_err(|err| RunError::FailedToSubscribeToOrderState(err))?;
        info!("Brokerage stream started");
        let mut check_subscribers_interval = tokio::time::interval(
            tokio::time::Duration::from_secs(check_subscribers_interval.num_seconds() as u64),
        );
        loop {
            futures::select! {
                res = stream.next().fuse() => {
                    let mut stop = false;
                    for stream_item in res.map_err(|err| RunError::FailedToGetStreamItems(err))?.into_iter() {
                        if handle_stream_item(&mut order_state_tx, stream_item).await && no_subscribers(&order_state_tx) {
                            stop = true;
                            break;
                        };
                    }
                    if stop {
                        break;
                    }
                },
                res = subscribe_to_order_state_requests.next().fuse() => match res {
                    Some(dst) => {
                        let _ = dst.send(order_state_tx.subscribe());
                    },
                    None => {
                        return Ok(false);
                    }
                },
                _ = check_subscribers_interval.tick().fuse() => {
                    if no_subscribers(&order_state_tx) {
                        break;
                    }
                }
            };
        }
        stream
            .shutdown()
            .await
            .map_err(|err| RunError::FailedToShutdownStream(err))?;
        info!("Brokerage stream shutdown");
        Ok(true)
    }

    pub fn new(
        brokerage: Arc<B>,
        check_subscribers_interval: Duration,
    ) -> (Self, impl Future<Output = Result<(), RunError<BE, BSE>>>) {
        let (subscribe_to_order_state_requests_tx, subscribe_to_order_state_requests_rx) =
            mpsc::channel(4);
        let run_task = {
            let brokerage = Arc::clone(&brokerage);
            async move {
                Self::run(
                    brokerage.as_ref(),
                    subscribe_to_order_state_requests_rx,
                    check_subscribers_interval,
                )
                .await
            }
        };
        (
            Self {
                subscribe_to_order_state_requests: subscribe_to_order_state_requests_tx,
            },
            run_task,
        )
    }

    pub async fn subscribe_to_order_states(
        &self,
    ) -> Result<broadcast::Receiver<(OID, OrderState)>, SharedStreamError> {
        let (tx, rx) = oneshot::channel();
        self.subscribe_to_order_state_requests
            .clone()
            .send(tx)
            .await
            .map_err(|err| SharedStreamError::FailedToSendOrderStateSubscriptionRequest(err))?;
        let stream = rx
            .await
            .map_err(|err| SharedStreamError::FailedToGetOrderStateStream(err))?;
        Ok(stream)
    }
}

#[async_trait]
pub trait Brokerage {
    type Error: Send + Sync + 'static;
    type OrderId: Send + Sync + 'static;
    type Stream: Stream<OrderId = Self::OrderId> + Send + Sync + 'static;

    async fn get_bars(
        &self,
        underlying: StockSymbol,
        start: DateTime<Utc>,
        end: DateTime<Utc>,
        period: Period,
    ) -> Result<Vec<Bar>, Self::Error>;

    async fn get_stream(&self) -> Result<Self::Stream, Self::Error>;

    async fn get_options_contracts<T: Datelike + Clone + Serialize + Send + Sync + 'static>(
        &self,
        underlying: StockSymbol,
        expiration: T,
    ) -> Result<(Vec<OptionsContract>, Vec<OptionsContract>), Self::Error>;

    async fn get_order_state(&self, order_id: Self::OrderId) -> Result<OrderState, Self::Error>;

    async fn place_options_strategy_order(
        &self,
        underlying: StockSymbol,
        options_strategy: &OptionsStrategy,
        close: bool,
        quantity: u32,
        price: Option<Microdollar>,
    ) -> Result<Self::OrderId, Self::Error>;

    fn get_required_buying_power(
        underlying: StockSymbol,
        options_strategy: &OptionsStrategy,
        price: Microdollar,
        quantity: u32,
    ) -> Microdollar;

    async fn cancel_order_if_exists(&self, order_id: Self::OrderId) -> Result<bool, Self::Error>;
}
