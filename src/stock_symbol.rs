use {
    arrayvec::ArrayString,
    bytes::BytesMut,
    serde::{
        de::{Deserializer, Visitor},
        Deserialize, Serialize, Serializer,
    },
    std::{
        convert::TryFrom,
        fmt::{self, Display, Formatter},
    },
    thiserror::Error,
    tokio_postgres::types::{IsNull, ToSql, Type},
};

#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct StockSymbol(ArrayString<5>);

#[derive(Error, Debug)]
pub enum StockSymbolError {
    #[error("Stock symbol character count, {0}, is not less than or equal to 5")]
    StockSymbolTooLong(usize),
    #[error("Invalid character '{0}' at position {1}: All characters must be alphabet characters")]
    InvalidCharacter(usize, char),
}

impl StockSymbol {
    pub fn new(stock_symbol: &str) -> Result<Self, StockSymbolError> {
        let stock_symbol = stock_symbol.to_uppercase();
        if stock_symbol.len() > 5 {
            return Err(StockSymbolError::StockSymbolTooLong(stock_symbol.len()));
        }
        for (i, c) in stock_symbol.chars().enumerate() {
            if c < 'A' || c > 'Z' {
                return Err(StockSymbolError::InvalidCharacter(i, c));
            }
        }
        let mut tmp = ArrayString::new();
        tmp.push_str(&stock_symbol);
        Ok(Self(tmp))
    }

    pub fn to_upper(&self) -> String {
        self.0.to_uppercase()
    }
}

impl AsRef<str> for StockSymbol {
    fn as_ref(&self) -> &str {
        &self.0
    }
}

impl Display for StockSymbol {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        self.0.fmt(f)
    }
}

impl ToSql for StockSymbol {
    fn to_sql(
        &self,
        ty: &Type,
        out: &mut BytesMut,
    ) -> Result<IsNull, Box<dyn std::error::Error + 'static + Send + Sync>> {
        self.0.as_str().to_sql(ty, out)
    }

    fn accepts(ty: &Type) -> bool {
        <&str as ToSql>::accepts(ty)
    }

    tokio_postgres::types::to_sql_checked!();
}

impl TryFrom<&str> for StockSymbol {
    type Error = StockSymbolError;

    fn try_from(value: &str) -> Result<Self, Self::Error> {
        Self::new(value)
    }
}

impl<'de> Deserialize<'de> for StockSymbol {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct StockSymbolVisitor;

        impl<'de> Visitor<'de> for StockSymbolVisitor {
            type Value = StockSymbol;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a floating point number")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                StockSymbol::try_from(v).map_err(|e| E::custom(format!("{}", e)))
            }
        }

        deserializer.deserialize_any(StockSymbolVisitor)
    }
}

impl Serialize for StockSymbol {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_str(self.as_ref())
    }
}
