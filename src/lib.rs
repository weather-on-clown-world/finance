use chrono::Timelike;

pub mod brokerage;
pub mod models;
pub mod options_strategy;

mod bar_cache;
mod microdollar;
mod stock_symbol;

pub use {
    bar_cache::BarCache,
    brokerage::{Brokerage, OrderState, SharedStream, Stream, StreamItem},
    microdollar::Microdollar,
    options_strategy::OptionsStrategy,
    stock_symbol::{StockSymbol, StockSymbolError},
    thiserror::Error,
};

use {
    chrono::{DateTime, Datelike, Duration, NaiveDate, NaiveTime, TimeZone, Utc, Weekday},
    chrono_tz::{Tz, US::Eastern},
    serde::{Deserialize, Serialize},
    std::{
        cmp::{Ord, Ordering},
        fmt,
    },
    tokio_postgres::types::Type,
};

#[derive(Debug, Clone, Copy, Hash, Eq, PartialEq)]
pub enum Period {
    OneMinute,
    FiveMinutes,
    FifteenMinutes,
    OneHour,
    OneDay,
}

impl fmt::Display for Period {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            Self::OneMinute => write!(f, "1min"),
            Self::FiveMinutes => write!(f, "5min"),
            Self::FifteenMinutes => write!(f, "15min"),
            Self::OneHour => write!(f, "1hour"),
            Self::OneDay => write!(f, "1day"),
        }
    }
}

pub const MAX_1_MIN_BARS_IN_DAY: usize = 390;
pub const MAX_5_MIN_BARS_IN_DAY: usize = 78;
pub const MAX_15_MIN_BARS_IN_DAY: usize = 26;

pub const OPTIONS_CONTRACT_CREATE_TYPE_STATEMENT: &str = "CREATE TYPE options_contract AS (      \
                                                             quote_datetime TIMESTAMPTZ NOT NULL, \
                                                             expiration DATE NOT NULL,            \
                                                             strike BIGINT NOT NULL,              \
                                                             open BIGINT NOT NULL,                \
                                                             high BIGINT NOT NULL,                \
                                                             low BIGINT NOT NULL,                 \
                                                             close BIGINT NOT NULL,               \
                                                             trade_volume BIGINT NOT NULL,        \
                                                             bid_size BIGINT NOT NULL,            \
                                                             bid BIGINT NOT NULL,                 \
                                                             ask_size BIGINT NOT NULL,            \
                                                             ask BIGINT NOT NULL,                 \
                                                             implied_volatility REAL NOT NULL,    \
                                                             delta REAL NOT NULL,                 \
                                                             gamma REAL NOT NULL,                 \
                                                             theta REAL NOT NULL,                 \
                                                             vega REAL NOT NULL,                  \
                                                             rho REAL NOT NULL,                   \
                                                             open_interest BIGINT NOT NULL,       \
                                                             is_call BOOLEAN NOT NULL,            \
                                                         );";

pub const OPTIONS_CONTRACT_CREATE_TABLE_STATMENT_BODY: &str = "quote_datetime TIMESTAMPTZ NOT NULL, \
                                                               expiration DATE NOT NULL,            \
                                                               strike BIGINT NOT NULL,              \
                                                               open BIGINT NOT NULL,                \
                                                               high BIGINT NOT NULL,                \
                                                               low BIGINT NOT NULL,                 \
                                                               close BIGINT NOT NULL,               \
                                                               trade_volume BIGINT NOT NULL,        \
                                                               bid_size BIGINT NOT NULL,            \
                                                               bid BIGINT NOT NULL,                 \
                                                               ask_size BIGINT NOT NULL,            \
                                                               ask BIGINT NOT NULL,                 \
                                                               implied_volatility REAL NOT NULL,    \
                                                               delta REAL NOT NULL,                 \
                                                               gamma REAL NOT NULL,                 \
                                                               theta REAL NOT NULL,                 \
                                                               vega REAL NOT NULL,                  \
                                                               rho REAL NOT NULL,                   \
                                                               open_interest BIGINT NOT NULL,       \
                                                               is_call BOOLEAN NOT NULL,            \
                                                               PRIMARY KEY(quote_datetime, expiration, strike, is_call)";

pub const OPTIONS_CONTRACT_TABLE_COPY_QUERY_TYPES: [Type; 20] = [
    Type::TIMESTAMPTZ, // quote datetime
    Type::DATE,        // expiration
    Type::INT8,        // strike
    Type::INT8,        // open
    Type::INT8,        // high
    Type::INT8,        // low
    Type::INT8,        // close
    Type::INT8,        // trade volume
    Type::INT8,        // bid size
    Type::INT8,        // bid
    Type::INT8,        // ask_size
    Type::INT8,        // ask
    Type::FLOAT4,      // implied volatility
    Type::FLOAT4,      // delta
    Type::FLOAT4,      // gamma
    Type::FLOAT4,      // theta
    Type::FLOAT4,      // vega
    Type::FLOAT4,      // rho
    Type::INT8,        // open interest
    Type::BOOL,        // is_call
];

pub fn create_options_contract_table_copy_query(table_name: &str, schema_name: &str) -> String {
    format!(
        "COPY {}.{} (         \
          quote_datetime,     \
          expiration,         \
          strike,             \
          open,               \
          high,               \
          low,                \
          close,              \
          trade_volume,       \
          bid_size,           \
          bid,                \
          ask_size,           \
          ask,                \
          implied_volatility, \
          delta,              \
          gamma,              \
          theta,              \
          vega,               \
          rho,                \
          open_interest,      \
          is_call             \
      ) FROM STDIN BINARY;",
        schema_name, table_name
    )
}

pub fn create_stock_bars_table_copy_query(table_name: &str) -> String {
    format!(
        "COPY {} (      \
           quote_datetime, \
           open,           \
           high,           \
           low,            \
           close,          \
           trade_volume    \
        ) FROM STDIN BINARY;",
        table_name
    )
}

pub const BAR_TABLE_COPY_QUERY_TYPES: [Type; 6] = [
    Type::TIMESTAMPTZ, // quote datetime
    Type::INT8,        // open
    Type::INT8,        // high
    Type::INT8,        // low
    Type::INT8,        // close
    Type::INT8,        // trade volume
];

pub const STOCK_BARS_CREATE_TABLE_STATMENT_BODY: &str = "quote_datetime TIMESTAMPTZ NOT NULL, \
                                                         open BIGINT NOT NULL,                \
                                                         high BIGINT NOT NULL,                \
                                                         low BIGINT NOT NULL,                 \
                                                         close BIGINT NOT NULL,               \
                                                         trade_volume BIGINT NOT NULL,        \
                                                         PRIMARY KEY(quote_datetime)";

pub fn stock_market_opening_time_eastern() -> NaiveTime {
    NaiveTime::from_hms_opt(9, 30, 0).unwrap()
}

pub fn stock_market_closing_time_eastern() -> NaiveTime {
    NaiveTime::from_hms_opt(16, 0, 0).unwrap()
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, PartialEq)]
pub struct Bar {
    pub quote_datetime: DateTime<Utc>,
    pub open: Microdollar,
    pub high: Microdollar,
    pub low: Microdollar,
    pub close: Microdollar,
    pub trade_volume: u64,
}

impl Default for Bar {
    fn default() -> Self {
        Self {
            quote_datetime: Utc.with_ymd_and_hms(0, 1, 1, 0, 0, 0).unwrap(),
            open: Microdollar::default(),
            high: Microdollar::default(),
            low: Microdollar::default(),
            close: Microdollar::default(),
            trade_volume: u64::default(),
        }
    }
}

pub const STOCK_MARKET_OPEN_DURATION_MINUTES: i64 = 450;

pub fn stock_market_open_duration() -> Duration {
    Duration::minutes(STOCK_MARKET_OPEN_DURATION_MINUTES)
}

pub fn is_stock_market_open_this_date<T>(date: T) -> bool
where
    T: Datelike,
{
    fn is_nth_weekday<T>(date: &T, weekday: Weekday, n: usize) -> bool
    where
        T: Datelike,
    {
        let d = date.day() as usize;
        date.weekday() == weekday && (d - 1) / 7 == n - 1
    }

    fn easter_sunday(year: u32) -> u32 {
        let a = year % 19;
        let b = year / 100;
        let c = (b - (b / 4) - ((8 * b + 13) / 25) + (19 * a) + 15) % 30;
        let d = c - (c / 28) * (1 - (c / 28) * (29 / (c + 1)) * ((21 - a) / 11));
        let e = d - ((year + (year / 4) + d + 2 - b + (b / 4)) % 7);
        let month = 3 + ((e + 40) / 44);
        let day = e + 28 - (31 * (month / 4));
        day
    }

    // Weekends
    let weekday = date.weekday();
    if weekday == Weekday::Sat || weekday == Weekday::Sun {
        return false;
    }

    let day = date.day();
    let month = date.month();

    // If a holiday falls on a Saturday, the market will close on the preceding Friday
    // If a holiday falls on a Sunday, the market will close on the subsequent Monday

    // New Years
    if month == 1 && day == 1 {
        return false;
    }

    // Martin Luther King Jr. Day
    if month == 1 && is_nth_weekday(&date, Weekday::Mon, 3) {
        return false;
    }

    // President's Day
    if month == 2 && is_nth_weekday(&date, Weekday::Mon, 3) {
        return false;
    }

    // Good Friday
    if month == 4 && weekday == Weekday::Fri && day + 2 == easter_sunday(date.year() as u32) {
        return false;
    }

    // Memorial Day
    if month == 5 && weekday == Weekday::Mon && day >= 25 {
        return false;
    }

    // Independence Day
    if month == 7 {
        if day == 4
            || (weekday == Weekday::Fri && day == 3)
            || (weekday == Weekday::Mon && day == 5)
        {
            return false;
        }
    }

    // Labor Day
    if month == 9 && is_nth_weekday(&date, Weekday::Mon, 1) {
        return false;
    }

    // Thanksgiving
    if month == 11 && is_nth_weekday(&date, Weekday::Thu, 4) {
        return false;
    }

    // Christmas
    if month == 12 && day == 25 {
        return false;
    }

    true
}

pub fn is_stock_market_open_this_datetime<Tz: TimeZone>(datetime: DateTime<Tz>) -> bool {
    let datetime = datetime.with_timezone(&Eastern);
    if !is_stock_market_open_this_date(datetime.date_naive()) {
        false
    } else {
        let time = datetime.time();
        time >= stock_market_opening_time_eastern() && time < stock_market_closing_time_eastern()
    }
}

pub fn next_date_stock_market_is_open<T>(date: T, num_business_days: usize) -> NaiveDate
where
    T: Datelike,
{
    let mut next_date = NaiveDate::from_yo_opt(date.year(), date.ordinal()).unwrap();
    let mut remaining_business_days = num_business_days;
    loop {
        while !is_stock_market_open_this_date(next_date) {
            next_date += Duration::days(1);
        }
        if remaining_business_days == 0 {
            break;
        }
        next_date += Duration::days(1);
        remaining_business_days -= 1;
    }
    next_date
}

#[derive(thiserror::Error, Debug)]
pub enum Error {
    #[error("Offset is too large")]
    OffsetTooLarge,
}

pub fn next_datetime_stock_market_is_open_eastern<Tz1: TimeZone>(
    on: DateTime<Tz1>,
    offset: Duration,
) -> Result<DateTime<Tz>, Error> {
    if offset.num_minutes() > STOCK_MARKET_OPEN_DURATION_MINUTES {
        return Err(Error::OffsetTooLarge);
    }

    let next = (on + offset).with_timezone(&Eastern);
    let datetime = if is_stock_market_open_this_datetime(next) {
        next
    } else {
        Eastern
            .with_ymd_and_hms(
                next.year(),
                next.month(),
                next.day(),
                next.hour(),
                next.minute(),
                next.second(),
            )
            .unwrap()
    };
    Ok(datetime)
}

/// If `time` is earlier in the day than the stock market's open time then
/// return stock market's open time. If `time` is later in the day than the
/// stock market's close time then return stock market's close time. Else return
/// `time`.
pub fn clamp_to_stock_market_open_time_eastern_range(time: NaiveTime) -> NaiveTime {
    let opening_time = stock_market_opening_time_eastern();
    if time < opening_time {
        return opening_time;
    }

    // This is a little strange since the closing time isn't a time when the
    // stock market is open
    let closing_time = stock_market_opening_time_eastern();
    if time >= closing_time {
        return closing_time;
    }

    time
}

#[derive(Clone, Copy, Debug, PartialEq, Deserialize, Serialize)]
pub enum OptionsContractExpirationType {
    Weekly,
    Monthly,
    Other,
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub struct OptionsContract {
    pub quote_datetime: DateTime<Utc>,
    pub expiration: NaiveDate,
    pub expiration_type: OptionsContractExpirationType,
    pub strike_price: Microdollar,
    pub is_call: bool,
    pub trade_volume: u64,
    pub bid_size: u64,
    pub bid_price: Microdollar,
    pub ask_size: u64,
    pub ask_price: Microdollar,
    pub implied_volatility: f32,
    pub delta: f32,
    pub gamma: f32,
    pub theta: f32,
    pub vega: f32,
    pub rho: f32,
    pub open_interest: u64,
}

impl OptionsContract {
    pub fn occ_option_symbol(&self, symbol: StockSymbol) -> String {
        format!(
            "{:06}{:02}{:02}{:02}{}{:08}",
            symbol,
            self.expiration.year() % 100,
            self.expiration.month(),
            self.expiration.day(),
            if self.is_call { 'C' } else { 'P' },
            self.strike_price.value() / 1000,
        )
    }

    pub fn price(&self) -> Microdollar {
        (self.bid_price + self.ask_price) / 2
    }

    pub fn aggregate_price(shorts: &[OptionsContract], longs: &[OptionsContract]) -> Microdollar {
        let long_price: Microdollar = longs.iter().map(|long| long.price()).sum();
        let short_price: Microdollar = shorts.iter().map(|short| short.price()).sum();
        long_price - short_price
    }
}

impl Ord for OptionsContract {
    fn cmp(&self, other: &Self) -> Ordering {
        (self.strike_price, self.expiration, self.is_call).cmp(&(
            other.strike_price,
            other.expiration,
            other.is_call,
        ))
    }
}

impl PartialOrd for OptionsContract {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for OptionsContract {
    fn eq(&self, other: &Self) -> bool {
        (self.strike_price, self.expiration, self.is_call)
            == (other.strike_price, other.expiration, other.is_call)
    }
}

impl Eq for OptionsContract {}

#[derive(Debug, Clone, Copy, PartialEq)]
pub struct Quote {
    /// Current best bid price
    pub bid_price: Microdollar,
    // Current best ask price.
    pub ask_price: Microdollar,
    /// Price at which the last trade was matched.
    pub last_price: Microdollar,
    /// Aggregated shares traded throughout the day, includig pre/post market hours.
    pub volume: u64,
    /// Trade time of the last trade.
    pub trade_time: DateTime<Utc>,
    /// Trade time of the last quote.
    pub quote_time: DateTime<Utc>,
    /// Day's high trade price.
    pub high_price: Microdollar,
    /// Day's low trade price
    pub low_price: Microdollar,
    /// Previouse day's closing price.
    pub close_price: Microdollar,
    /// Option risk/volatility measurement.
    pub volatility: f32,
    /// Day's open price.
    pub open_price: Microdollar,
}

#[cfg(test)]
mod tests {
    use {
        super::*,
        chrono::{DateTime, Duration, NaiveDate, NaiveDateTime, NaiveTime, Utc},
    };

    macro_rules! from_ymd_hm {
        ($year:expr, $month:expr, $day:expr, $hour:expr, $minute:expr) => {
            DateTime::<Utc>::from_utc(
                NaiveDateTime::new(
                    NaiveDate::from_ymd($year, $month, $day),
                    NaiveTime::from_hms($hour, $minute, 0),
                ),
                Utc,
            )
        };
    }

    #[test]
    fn test_occ_option_symbol() {
        // Put on SPX, expiring on 11/22/2014, with a strike price of $19.50.
        let option = OptionsContract {
            quote_datetime: Utc::now(),
            expiration: NaiveDate::from_ymd(2014, 11, 22),
            expiration_type: OptionsContractExpirationType::Weekly,
            strike_price: Microdollar::from(19.50),
            is_call: false,
            trade_volume: 0,
            bid_size: 0,
            bid_price: Microdollar::from(0.0),
            ask_size: 0,
            ask_price: Microdollar::from(0.0),
            implied_volatility: 0.0,
            delta: 0.0,
            gamma: 0.0,
            theta: 0.0,
            vega: 0.0,
            rho: 0.0,
            open_interest: 0,
        };
        let symbol = StockSymbol::new("spx").unwrap();
        assert_eq!(&option.occ_option_symbol(symbol), "SPX   141122P00019500");

        // Call on LAMR, expiring on 1/17/2015, with a strike price of $52.50.
        let option = OptionsContract {
            quote_datetime: Utc::now(),
            expiration: NaiveDate::from_ymd(2015, 1, 17),
            expiration_type: OptionsContractExpirationType::Weekly,
            strike_price: Microdollar::from(52.50),
            is_call: true,
            trade_volume: 0,
            bid_size: 0,
            bid_price: Microdollar::from(0.0),
            ask_size: 0,
            ask_price: Microdollar::from(0.0),
            implied_volatility: 0.0,
            delta: 0.0,
            gamma: 0.0,
            theta: 0.0,
            vega: 0.0,
            rho: 0.0,
            open_interest: 0,
        };
        let symbol = StockSymbol::new("lamr").unwrap();
        assert_eq!(&option.occ_option_symbol(symbol), "LAMR  150117C00052500");
    }

    #[test]
    fn test_next_datetime_stock_market_is_open_eastern() {
        // Weekdays
        // - Open hours
        assert_eq!(
            next_datetime_stock_market_is_open_eastern(
                from_ymd_hm!(2001, 8, 1, 14, 44),
                Duration::minutes(2)
            )
            .unwrap(),
            from_ymd_hm!(2001, 8, 1, 14, 46)
        );
        assert_eq!(
            next_datetime_stock_market_is_open_eastern(
                from_ymd_hm!(2021, 7, 2, 16, 3),
                Duration::seconds(300)
            )
            .unwrap(),
            from_ymd_hm!(2021, 7, 2, 16, 8)
        );

        // Holidays
        // - Independence day
    }

    #[test]
    fn test_is_stock_market_open_this_datetime() {
        // Test UTC date times to make sure code is taking daylight savings
        // into account.

        // Weekends
        // - Open hours (false)
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2021, 6, 13, 13, 30
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2005, 1, 29, 17, 23
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2014, 4, 6, 19, 59
        )));
        // - Closed hours (false)
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2005, 1, 29, 12, 47
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2021, 6, 13, 14, 29
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2014, 4, 6, 21, 01
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2011, 11, 12, 23, 54
        )));

        // Non-holiday weekdays
        // - Open hours (true)
        assert!(is_stock_market_open_this_datetime(from_ymd_hm!(
            2021, 6, 14, 13, 30
        )));
        assert!(is_stock_market_open_this_datetime(from_ymd_hm!(
            2018, 9, 5, 17, 12
        )));
        assert!(is_stock_market_open_this_datetime(from_ymd_hm!(
            2001, 12, 3, 19, 59
        )));
        // - Closed hours (false)
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2021, 6, 14, 1, 49
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2019, 1, 8, 13, 29
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2010, 2, 1, 21, 00
        )));
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2004, 4, 30, 22, 20
        )));

        // Holidays on weekdays

        // - New years
        // -- Open hours (false)
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2021, 1, 1, 13, 30
        )));
        // -- Closed hours (false)
        assert!(!is_stock_market_open_this_datetime(from_ymd_hm!(
            2015, 1, 1, 23, 9
        )));

        // - Martin Luther King Jr. Day
        // -- Open hours (false)
        // -- Closed hours (false)

        // - President's Day
        // -- Open hours (false)
        // -- Closed hours (false)

        // - Good Friday
        // -- Open hours (false)
        // -- Closed hours (false)

        // - Memorial Day
        // -- Open hours (false)
        // -- Closed hours (false)

        // - Independence Day
        // -- Open hours (false)
        // -- Closed hours (false)

        // - Labor Day
        // -- Open hours (false)
        // -- Closed hours (false)

        // - Thanksgiving
        // -- Open hours (false)
        // -- Closed hours (false)

        // - Christmas
        // -- Open hours (false)
        // -- Closed hours (false)
    }
}
