use {
    crate::{Brokerage, Microdollar, OptionsContract, OptionsContractExpirationType, StockSymbol},
    chrono::{Duration, NaiveDate},
    serde::{Deserialize, Serialize},
    thiserror::Error,
};

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct Leg {
    options_contract: OptionsContract,
    is_short: bool,
}

impl Leg {
    pub fn new(options_contract: OptionsContract, is_short: bool) -> Self {
        Self {
            options_contract,
            is_short,
        }
    }

    pub fn price(&self) -> Microdollar {
        let price = (self.options_contract.bid_price + self.options_contract.ask_price) / 2;
        if self.is_short {
            -price
        } else {
            price
        }
    }

    pub fn is_short(&self) -> bool {
        self.is_short
    }

    pub fn options_contract(&self) -> &OptionsContract {
        &self.options_contract
    }
}

#[derive(Debug, Copy, Clone, Serialize, Deserialize)]
pub struct OptionsStrategy {
    leg_1: Leg,
    leg_2: Option<Leg>,
    leg_3: Option<Leg>,
    leg_4: Option<Leg>,
}

#[derive(Error, Debug)]
pub enum OptionsStrategyError<E: Send + Sync + 'static> {
    #[error("Failed to get options contracts from brokerage: {0}")]
    FailedToGetOptionsContracts(E),
    #[error("Could not find leg 1")]
    Leg1Missing,
    #[error("Could not find leg 2")]
    Leg2Missing,
    #[error("Could not find leg 3")]
    Leg3Missing,
    #[error("Could not find leg 4")]
    Leg4Missing,
}

impl OptionsStrategy {
    pub fn new(leg_1: Leg, leg_2: Option<Leg>, leg_3: Option<Leg>, leg_4: Option<Leg>) -> Self {
        Self {
            leg_1,
            leg_2,
            leg_3,
            leg_4,
        }
    }

    pub fn leg_1(&self) -> &Leg {
        &self.leg_1
    }

    pub fn leg_2(&self) -> Option<&Leg> {
        self.leg_2.as_ref()
    }

    pub fn leg_3(&self) -> Option<&Leg> {
        self.leg_3.as_ref()
    }

    pub fn leg_4(&self) -> Option<&Leg> {
        self.leg_4.as_ref()
    }

    pub fn price(&self) -> Microdollar {
        fn two_leg(leg_1: &Leg, leg_2: &Leg) -> Microdollar {
            leg_1.price() + leg_2.price()
        }
        fn three_leg(leg_1: &Leg, leg_2: &Leg, leg_3: &Leg) -> Microdollar {
            leg_1.price() + leg_2.price() + leg_3.price()
        }
        fn four_leg(leg_1: &Leg, leg_2: &Leg, leg_3: &Leg, leg_4: &Leg) -> Microdollar {
            leg_1.price() + leg_2.price() + leg_3.price() + leg_4.price()
        }

        match (self.leg_2, self.leg_3, self.leg_4) {
            (None, None, None) => self.leg_1.price(),
            (Some(ref leg_2), None, None)
            | (None, Some(ref leg_2), None)
            | (None, None, Some(ref leg_2)) => two_leg(&self.leg_1, leg_2),
            (Some(ref leg_2), Some(ref leg_3), None)
            | (Some(ref leg_2), None, Some(ref leg_3))
            | (None, Some(ref leg_2), Some(ref leg_3)) => three_leg(&self.leg_1, leg_2, leg_3),
            (Some(ref leg_2), Some(ref leg_3), Some(ref leg_4)) => {
                four_leg(&self.leg_1, leg_2, leg_3, leg_4)
            }
        }
    }

    pub fn max_loss(&self, price: Microdollar) -> Microdollar {
        fn one_leg(leg: &Leg, price: Microdollar) -> Microdollar {
            match (leg.is_short, leg.options_contract.is_call) {
                (true, true) => todo!("Infinity"),
                (true, false) => leg.options_contract.strike_price + price,
                (false, _) => price,
            }
        }

        fn two_leg(_leg_1: &Leg, _leg_2: &Leg) -> Microdollar {
            todo!()
        }
        fn three_leg(_leg_1: &Leg, _leg_2: &Leg, _leg_3: &Leg) -> Microdollar {
            todo!()
        }
        fn four_leg(
            leg_1: &Leg,
            leg_2: &Leg,
            leg_3: &Leg,
            leg_4: &Leg,
            price: Microdollar,
        ) -> Microdollar {
            fn iron_condor(
                long_put: &Leg,
                short_put: &Leg,
                short_call: &Leg,
                long_call: &Leg,
                price: Microdollar,
            ) -> Microdollar {
                let put_max_loss = short_put.options_contract.strike_price
                    - long_put.options_contract.strike_price;
                let call_max_loss = long_call.options_contract.strike_price
                    - short_call.options_contract.strike_price;

                (put_max_loss + call_max_loss) / 2 + price
            }

            match (
                (leg_1.is_short, leg_1.options_contract.is_call),
                (leg_2.is_short, leg_2.options_contract.is_call),
                (leg_3.is_short, leg_3.options_contract.is_call),
                (leg_4.is_short, leg_4.options_contract.is_call),
            ) {
                ((false, false), (true, false), (true, true), (false, true)) => {
                    iron_condor(leg_1, leg_2, leg_3, leg_4, price)
                }
                _ => todo!(),
            }
        }

        match (self.leg_2, self.leg_3, self.leg_4) {
            (None, None, None) => one_leg(&self.leg_1, price),
            (Some(ref leg_2), None, None)
            | (None, Some(ref leg_2), None)
            | (None, None, Some(ref leg_2)) => two_leg(&self.leg_1, leg_2),
            (Some(ref leg_2), Some(ref leg_3), None)
            | (Some(ref leg_2), None, Some(ref leg_3))
            | (None, Some(ref leg_2), Some(ref leg_3)) => three_leg(&self.leg_1, leg_2, leg_3),
            (Some(ref leg_2), Some(ref leg_3), Some(ref leg_4)) => {
                four_leg(&self.leg_1, leg_2, leg_3, leg_4, price)
            }
        }
    }

    pub fn expiration(&self) -> NaiveDate {
        self.leg_1.options_contract.expiration
    }

    pub fn expiration_type(&self) -> OptionsContractExpirationType {
        self.leg_1.options_contract.expiration_type
    }
    pub async fn get_current_price<B: Brokerage + Send + Sync + 'static>(
        &self,
        underlying: StockSymbol,
        brokerage: &B,
    ) -> Result<Microdollar, OptionsStrategyError<B::Error>> {
        fn get_leg<'a, 'b>(
            leg: &Leg,
            mut puts: impl Iterator<Item = &'a OptionsContract>,
            mut calls: impl Iterator<Item = &'b OptionsContract>,
        ) -> Option<Microdollar> {
            if leg.options_contract.is_call {
                calls.find(|options_contract| {
                    options_contract.strike_price == leg.options_contract.strike_price
                })
            } else {
                puts.find(|options_contract| {
                    options_contract.strike_price == leg.options_contract.strike_price
                })
            }
            .map(|options_contract| {
                let price = (options_contract.bid_price + options_contract.ask_price) / 2;
                if leg.is_short {
                    -price
                } else {
                    price
                }
            })
        }

        let (puts, calls) = brokerage
            .get_options_contracts(underlying, self.expiration())
            .await
            .map_err(|err| OptionsStrategyError::FailedToGetOptionsContracts(err))?;

        let puts = puts.iter().filter(|options_contract| {
            options_contract.expiration == self.expiration()
                && options_contract.expiration_type == self.expiration_type()
        });
        let calls = calls.iter().filter(|options_contract| {
            options_contract.expiration == self.expiration()
                && options_contract.expiration_type == self.expiration_type()
        });

        let mut price = get_leg(&self.leg_1, puts.clone(), calls.clone())
            .ok_or(OptionsStrategyError::Leg1Missing)?;
        if let Some(ref leg) = self.leg_4 {
            price += get_leg(leg, puts.clone(), calls.clone())
                .ok_or(OptionsStrategyError::Leg4Missing)?;
        }
        if let Some(ref leg) = self.leg_3 {
            price += get_leg(leg, puts.clone(), calls.clone())
                .ok_or(OptionsStrategyError::Leg3Missing)?;
        }
        if let Some(ref leg) = self.leg_2 {
            price += get_leg(leg, puts, calls).ok_or(OptionsStrategyError::Leg2Missing)?;
        }

        Ok(price)
    }
}

pub async fn wait_until_options_strategy_is_below_price<B: Brokerage + Send + Sync + 'static>(
    underlying: StockSymbol,
    options_strategy: &OptionsStrategy,
    price: Microdollar,
    brokerage: &B,
    check_interval_period: Duration,
) -> Result<(), OptionsStrategyError<B::Error>> {
    let mut check_interval = tokio::time::interval(tokio::time::Duration::from_secs(
        check_interval_period.num_seconds() as u64,
    ));
    loop {
        check_interval.tick().await;
        let current_price = options_strategy
            .get_current_price(underlying, brokerage)
            .await?;
        tracing::info!("Current price of options strategy: {:#}", current_price);
        if current_price <= price {
            return Ok(());
        }
    }
}
#[cfg(test)]
mod tests {
    use {super::*, async_trait::async_trait, chrono::DateTime, chrono::Utc};

    #[derive(Error, Debug)]
    pub enum TestStreamError {}

    struct TestStream;

    #[async_trait]
    impl crate::Stream for TestStream {
        type Error = TestStreamError;

        type OrderId = u64;

        async fn subscribe_to_minute_bars(
            &mut self,
            _underlyings: &[StockSymbol],
        ) -> Result<(), Self::Error> {
            todo!()
        }

        async fn subscribe_to_quotes(
            &mut self,
            _underlyings: &[StockSymbol],
        ) -> Result<(), Self::Error> {
            todo!()
        }

        async fn subscribe_to_options_contracts(
            &mut self,
            _underlyings: &[StockSymbol],
        ) -> Result<(), Self::Error> {
            todo!()
        }

        async fn subscribe_to_order_states(&mut self) -> Result<(), Self::Error> {
            todo!()
        }

        async fn next(&mut self) -> Result<Vec<crate::StreamItem<Self::OrderId>>, Self::Error> {
            todo!()
        }

        async fn shutdown(self) -> Result<(), Self::Error>
        where
            Self:,
        {
            todo!()
        }
    }

    #[derive(Error, Debug)]
    pub enum TestBrokerageError {}

    struct TestBrokerage {
        puts: Vec<OptionsContract>,
        calls: Vec<OptionsContract>,
    }

    #[async_trait]
    impl Brokerage for TestBrokerage {
        type Error = TestBrokerageError;

        type OrderId = u64;

        type Stream = TestStream;

        async fn get_bars(
            &self,
            _underlying: StockSymbol,
            _start: DateTime<Utc>,
            _end: DateTime<Utc>,
            _period: crate::Period,
        ) -> Result<Vec<crate::Bar>, Self::Error> {
            todo!()
        }

        async fn get_stream(&self) -> Result<Self::Stream, Self::Error> {
            todo!()
        }

        async fn get_options_contracts<T>(
            &self,
            _underlying: StockSymbol,
            _expiration: T,
        ) -> Result<(Vec<OptionsContract>, Vec<OptionsContract>), Self::Error>
        where
            T: chrono::Datelike + Clone + Serialize + Send + Sync,
        {
            Ok((self.puts.clone(), self.calls.clone()))
        }

        async fn get_order_state(
            &self,
            _order_id: Self::OrderId,
        ) -> Result<crate::OrderState, Self::Error> {
            todo!()
        }

        async fn place_options_strategy_order(
            &self,
            _underlying: StockSymbol,
            _options_strategy: &OptionsStrategy,
            _close: bool,
            _quantity: u32,
            _price: Option<Microdollar>,
        ) -> Result<Self::OrderId, Self::Error> {
            todo!()
        }

        async fn cancel_order_if_exists(
            &self,
            _order_id: Self::OrderId,
        ) -> Result<bool, Self::Error> {
            todo!()
        }

        fn get_required_buying_power(
            _underlying: StockSymbol,
            _options_strategy: &OptionsStrategy,
            _price: Microdollar,
            _quantity: u32,
        ) -> Microdollar {
            todo!()
        }
    }

    fn options_contract(
        expiration: NaiveDate,
        expiration_type: OptionsContractExpirationType,
        strike_price: Microdollar,
        is_call: bool,
        bid_price: Microdollar,
        ask_price: Microdollar,
    ) -> OptionsContract {
        OptionsContract {
            quote_datetime: Utc::now(),
            expiration,
            expiration_type,
            strike_price,
            is_call,
            trade_volume: 0,
            bid_size: 0,
            bid_price,
            ask_size: 0,
            ask_price,
            implied_volatility: 0.0,
            delta: 0.0,
            gamma: 0.0,
            theta: 0.0,
            vega: 0.0,
            rho: 0.0,
            open_interest: 0,
        }
    }

    #[tokio::test]
    async fn options_strategy_one_long_leg() {
        let expiration = Utc::now().date_naive();

        let brokerage = TestBrokerage {
            puts: vec![
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Weekly,
                    4135.0.into(),
                    false,
                    15.0.into(),
                    15.0.into(),
                ),
                options_contract(
                    expiration + Duration::days(1),
                    OptionsContractExpirationType::Weekly,
                    4135.0.into(),
                    false,
                    16.0.into(),
                    16.0.into(),
                ),
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4135.0.into(),
                    false,
                    13.1.into(),
                    13.2.into(),
                ),
            ],
            calls: vec![],
        };

        let options_strategy = OptionsStrategy::new(
            Leg::new(
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4135.0.into(),
                    false,
                    12.6.into(),
                    13.2.into(),
                ),
                false,
            ),
            None,
            None,
            None,
        );

        assert_eq!(options_strategy.expiration(), expiration);
        assert_eq!(
            options_strategy.expiration_type(),
            OptionsContractExpirationType::Monthly
        );
        assert_eq!(options_strategy.price(), Microdollar::from(12.9));
        assert_eq!(
            options_strategy
                .get_current_price(StockSymbol::new("spx").unwrap(), &brokerage)
                .await
                .unwrap(),
            Microdollar::from(13.15)
        );
    }

    #[tokio::test]
    async fn options_strategy_one_short_leg() {
        let expiration = Utc::now().date_naive();

        let brokerage = TestBrokerage {
            puts: vec![],
            calls: vec![options_contract(
                expiration.clone(),
                OptionsContractExpirationType::Weekly,
                4135.0.into(),
                true,
                13.3.into(),
                13.5.into(),
            )],
        };

        let options_strategy = OptionsStrategy::new(
            Leg::new(
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Weekly,
                    4135.0.into(),
                    true,
                    12.6.into(),
                    13.2.into(),
                ),
                true,
            ),
            None,
            None,
            None,
        );
        assert_eq!(options_strategy.expiration(), expiration);
        assert_eq!(
            options_strategy.expiration_type(),
            OptionsContractExpirationType::Weekly
        );
        assert_eq!(options_strategy.price(), Microdollar::from(-12.9));
        assert_eq!(
            options_strategy
                .get_current_price(StockSymbol::new("spx").unwrap(), &brokerage)
                .await
                .unwrap(),
            Microdollar::from(-13.4)
        );
    }

    #[tokio::test]
    async fn options_strategy_iron_condor() {
        let expiration = Utc::now().date_naive();
        let brokerage = TestBrokerage {
            puts: vec![
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4135.0.into(),
                    false,
                    0.3.into(),
                    0.5.into(),
                ),
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4235.0.into(),
                    false,
                    10.0.into(),
                    11.0.into(),
                ),
            ],
            calls: vec![
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4400.0.into(),
                    true,
                    9.0.into(),
                    9.4.into(),
                ),
                options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4500.0.into(),
                    true,
                    0.6.into(),
                    0.8.into(),
                ),
            ],
        };

        let options_strategy = OptionsStrategy {
            leg_1: Leg {
                options_contract: options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4135.0.into(),
                    false,
                    5.2.into(),
                    5.6.into(),
                ),
                is_short: false,
            },
            leg_2: Some(Leg {
                options_contract: options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4235.0.into(),
                    false,
                    12.6.into(),
                    13.2.into(),
                ),
                is_short: true,
            }),
            leg_3: Some(Leg {
                options_contract: options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4400.0.into(),
                    true,
                    12.9.into(),
                    13.9.into(),
                ),
                is_short: true,
            }),
            leg_4: Some(Leg {
                options_contract: options_contract(
                    expiration.clone(),
                    OptionsContractExpirationType::Monthly,
                    4500.0.into(),
                    true,
                    2.0.into(),
                    3.0.into(),
                ),
                is_short: false,
            }),
        };

        assert_eq!(options_strategy.expiration(), expiration);
        assert_eq!(
            options_strategy.expiration_type(),
            OptionsContractExpirationType::Monthly
        );
        assert_eq!(options_strategy.price(), Microdollar::from(-18.4));
        assert_eq!(
            options_strategy
                .get_current_price(StockSymbol::new("spx").unwrap(), &brokerage)
                .await
                .unwrap(),
            Microdollar::from(-18.6)
        );
    }
}
