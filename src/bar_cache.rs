use {
    crate::Bar,
    chrono::{DateTime, NaiveDate, Utc},
    futures::{
        sink::{Sink, SinkExt},
        stream,
    },
    lru::LruCache,
    std::fmt::Display,
    thiserror::Error,
};

#[derive(Error, Debug)]
pub enum Error {
    #[error("Failed to send bars to sink: {0}")]
    FailedToSendBars(String),
    #[error("Current bars collection is full")]
    CurrentBarsFull,
    #[error("Bar's date does not match the current set date, {expected:}")]
    IncorrectDate { expected: NaiveDate },
    #[error("New current date must be after the existing current date, {minimum:}")]
    NewCurrentDateTooEarly { minimum: NaiveDate },
    #[error("The number of current bars must be less than the maximum capcity, {maximum:}")]
    NumCurrentBarsTooLarge { maximum: usize },
    #[error("The date of the bar at position {i:} of current bars is not the current date")]
    CurrBarIncorrectDate { i: usize },
    #[error("Current bars not sorted by their quote datetime from earliest to latest")]
    CurrBarsNotSortedByDatetime,
    #[error("The new last current bar's quote timestamp must be greater than the second last current bar's quote timestamp, {minimum:}")]
    InvalidNewLastBarQuoteDateTime { minimum: DateTime<Utc> },
}

pub struct BarCache<const N: usize> {
    curr: NaiveDate,
    curr_bars: [Bar; N],
    num_curr_bars: usize,
    bars: LruCache<NaiveDate, (usize, [Bar; N])>,
}

impl<const N: usize> BarCache<N> {
    pub fn new(
        curr: NaiveDate,
        curr_bars: [Bar; N],
        num_curr_bars: usize,
        lru_capacity: usize,
    ) -> Result<Self, Error> {
        if num_curr_bars > N {
            return Err(Error::NumCurrentBarsTooLarge { maximum: N });
        }
        for i in 0..num_curr_bars {
            let bar = &curr_bars[i];
            if bar.quote_datetime.date_naive() != curr {
                return Err(Error::CurrBarIncorrectDate { i });
            }
            if i != 0 && curr_bars[i - 1].quote_datetime > bar.quote_datetime {
                return Err(Error::CurrBarsNotSortedByDatetime);
            }
        }
        Ok(Self {
            curr,
            curr_bars,
            num_curr_bars,
            bars: LruCache::new(std::num::NonZeroUsize::new(lru_capacity).unwrap()),
        })
    }

    pub fn update_last_curr(&mut self, bar: Bar) -> Result<(), Error> {
        if bar.quote_datetime.date_naive() != self.curr {
            return Err(Error::IncorrectDate {
                expected: self.curr,
            });
        }
        if self.num_curr_bars > 1
            && self.curr_bars[self.num_curr_bars - 2].quote_datetime > bar.quote_datetime
        {
            return Err(Error::InvalidNewLastBarQuoteDateTime {
                minimum: self.curr_bars[self.num_curr_bars - 2].quote_datetime,
            });
        }

        self.curr_bars[self.num_curr_bars] = bar;
        if self.num_curr_bars == 0 {
            self.num_curr_bars += 1
        }
        Ok(())
    }

    pub fn store(&mut self, date: NaiveDate, bars: [Bar; N], num_bars: usize) {
        self.bars.put(date, (num_bars, bars));
    }

    pub async fn load<S>(&mut self, date: &NaiveDate, dst: &mut S) -> Result<bool, Error>
    where
        S: Sink<Bar> + Unpin,
        <S as futures::Sink<Bar>>::Error: Display,
    {
        async fn send_bars<S>(bars: &[Bar], num_bars: usize, dst: &mut S) -> Result<(), Error>
        where
            S: Sink<Bar> + Unpin,
            <S as futures::Sink<Bar>>::Error: Display,
        {
            dst.send_all(&mut stream::iter(
                (0..num_bars).map(|i| Ok(bars[i].clone())),
            ))
            .await
            .map_err(|err| Error::FailedToSendBars(format!("{}", err)))
        }

        if date == &self.curr {
            send_bars(&self.curr_bars, self.num_curr_bars, dst).await?;
            Ok(true)
        } else if let Some((num_bars, bars)) = self.bars.get(date) {
            send_bars(bars, *num_bars, dst).await?;
            Ok(true)
        } else {
            Ok(false)
        }
    }

    pub async fn new_curr(&mut self, curr: NaiveDate) -> Result<(), Error> {
        if curr <= self.curr {
            return Err(Error::NewCurrentDateTooEarly { minimum: self.curr });
        }

        self.bars
            .push(self.curr, (self.num_curr_bars, self.curr_bars));
        self.curr = curr;
        self.num_curr_bars = 0;

        Ok(())
    }
}
