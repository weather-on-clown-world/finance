use {
    ringbuffer::{ConstGenericRingBuffer, RingBufferExt, RingBufferWrite},
    thiserror::Error,
};

/// `N` is the period of the middle band and the look-back period for the
/// standard deviation.
pub struct BollingerBands<const N: usize> {
    middle_band: Sma<N>,
    std_dev: StdDev<N>,
    coefficient: f32,
}

impl<const N: usize> BollingerBands<N> {
    pub fn new(
        data: &[f32],
        coefficient: f32,
    ) -> Result<Self, NewBollingerBandError> {
        let middle_band = Sma::new(data)
            .map_err(|e| NewBollingerBandError::FailedToCreateMiddleBand(e))?;
        let std_dev = StdDev::new(data)
            .map_err(|e| NewBollingerBandError::FailedToCreateStdDev(e))?;

        Ok(Self {
            middle_band,
            std_dev,
            coefficient,
        })
    }

    pub fn get_upper_and_lower_bands(&self) -> (f32, f32) {
        let tmp = self.std_dev.get_value() * self.coefficient;
        let middle_band = self.middle_band.get_value();
        (middle_band + tmp, middle_band - tmp)
    }

    pub fn update(&mut self, value: f32) -> (f32, f32) {
        let middle_band = self.middle_band.update(value);
        let tmp = self.std_dev.update(value) * self.coefficient;
        (middle_band + tmp, middle_band - tmp)
    }

    pub fn add(&mut self, value: f32) -> (f32, f32) {
        let middle_band = self.middle_band.add(value);
        let tmp = self.std_dev.add(value) * self.coefficient;
        (middle_band + tmp, middle_band - tmp)
    }
}

#[derive(Error, Debug)]
pub enum NewBollingerBandError {
    #[error("Failed to create middle band: {0}")]
    FailedToCreateMiddleBand(NewSmaError),
    #[error("Failed to create standard deviation: {0}")]
    FailedToCreateStdDev(NewStdDevError),
}

// Standard deviation of a sample (not a population). `N` is the sample size
pub struct StdDev<const N: usize> {
    value: f32,
    window: ConstGenericRingBuffer<f32, N>,
}

impl<const N: usize> StdDev<N> {
    fn calc_value(&mut self) -> f32 {
        let avg = self.window.iter().sum::<f32>() / (N as f32);
        self.value = 0.0;
        for x in self.window.iter() {
            let tmp = x - avg;
            self.value += tmp * tmp;
        }
        self.value = (self.value / (N as f32 - 1.0)).sqrt();
        self.value
    }

    pub fn new(data: &[f32]) -> Result<Self, NewStdDevError> {
        if N < 2 {
            return Err(NewStdDevError::PeriodTooSmall(N));
        }

        if data.len() < N {
            return Err(NewStdDevError::NotEnoughValues(N, data.len()));
        }

        let mut window = ConstGenericRingBuffer::<_, N>::new();
        for i in data.len() - N..data.len() {
            window.push(data[i]);
        }
        let mut std_dev = Self { value: 0.0, window };
        let _ = std_dev.calc_value();

        Ok(std_dev)
    }

    pub fn add(&mut self, value: f32) -> f32 {
        self.window.push(value);
        self.calc_value()
    }

    pub fn update(&mut self, value: f32) -> f32 {
        *self
            .window
            .back_mut()
            .expect("StdDev window missing a value") = value;
        self.calc_value()
    }

    pub fn get_value(&self) -> f32 {
        self.value
    }
}

#[derive(Error, Debug)]
pub enum NewStdDevError {
    #[error("Period is too small: Expected at least 2 but was {0}")]
    PeriodTooSmall(usize),
    #[error("Not enough data: Expected at least {0} but received {1}")]
    NotEnoughValues(usize, usize),
}

/// Simple moving average. `N` is the SMA's period. `N` must be greater than or
/// equal to 1.
pub struct Sma<const N: usize> {
    value: f32,
    window: ConstGenericRingBuffer<f32, N>,
}

impl<const N: usize> Sma<N> {
    fn calc_value(&mut self) -> f32 {
        self.value = self.window.iter().sum::<f32>() / (N as f32);
        self.value
    }

    pub fn new(data: &[f32]) -> Result<Self, NewSmaError> {
        if N < 1 {
            return Err(NewSmaError::PeriodTooSmall(N));
        }

        if data.len() < N {
            return Err(NewSmaError::NotEnoughValues(data.len(), N));
        }

        let mut window = ConstGenericRingBuffer::<_, N>::new();
        for i in data.len() - N..data.len() {
            window.push(data[i]);
        }
        let mut sma = Sma { value: 0.0, window };
        let _ = sma.calc_value();

        Ok(sma)
    }

    pub fn add(&mut self, value: f32) -> f32 {
        self.window.push(value);
        self.calc_value()
    }

    pub fn update(&mut self, value: f32) -> f32 {
        *self.window.back_mut().expect("SMA window missing a value") = value;
        self.calc_value()
    }

    pub fn get_value(&self) -> f32 {
        self.value
    }
}

#[derive(Error, Debug)]
pub enum NewSmaError {
    #[error("Period is too small: Expected at least 1 but was {0}")]
    PeriodTooSmall(usize),
    #[error("Not enough data: Expected at least {0} but received {1}")]
    NotEnoughValues(usize, usize),
}

/// Relative strength index. `N` is RSI's period. `N` must be greater than or
/// equal to 1.
pub struct Rsi<const N: usize> {
    value: f32,
    prev_avg_gain: f32,
    prev_avg_loss: f32,
    curr_price: f32,
    prev_price: f32,
}

impl<const N: usize> Rsi<N> {
    /// Creates a new RSI whose initial value is based on `close_prices`.
    /// `close_prices` should have at least `N` + 1 elements.
    pub fn new(close_prices: &[f32]) -> Result<Self, NewRsiError> {
        if N < 1 {
            return Err(NewRsiError::PeriodTooSmall(N));
        }

        if close_prices.len() < N + 1 {
            return Err(NewRsiError::NotEnoughClosePrices(
                close_prices.len(),
                N + 1,
            ));
        }

        // Calculate the initial average gain & average loss using the first `N`
        // open & close prices
        let mut prev_avg_gain = 0.0;
        let mut prev_avg_loss = 0.0;
        for i in 1..N + 1 {
            let price_delta = close_prices[i] - close_prices[i - 1];
            let gain = price_delta.max(0.0);
            let loss = -price_delta.min(0.0);
            prev_avg_gain += gain;
            prev_avg_loss += loss;
        }
        prev_avg_gain /= N as f32;
        prev_avg_loss /= N as f32;

        // Update RSI with the remaining open & close prices.
        for i in N + 1..close_prices.len() - 1 {
            let price_delta = close_prices[i] - close_prices[i - 1];
            let gain = price_delta.max(0.0);
            let loss = -price_delta.min(0.0);
            prev_avg_gain =
                (prev_avg_gain * (N as f32 - 1.0) + gain) / N as f32;
            prev_avg_loss =
                (prev_avg_loss * (N as f32 - 1.0) + loss) / N as f32;
        }

        let mut rsi = Self {
            value: 0.0,
            prev_avg_gain,
            prev_avg_loss,
            curr_price: close_prices[close_prices.len() - 1],
            prev_price: close_prices[close_prices.len() - 2],
        };
        let _ = rsi.calculate_value();

        Ok(rsi)
    }

    fn calculate_value(&mut self) -> f32 {
        let price_delta = self.curr_price - self.prev_price;

        let gain = price_delta.max(0.0);
        let avg_gain =
            (self.prev_avg_gain * (N as f32 - 1.0) + gain) / N as f32;

        let loss = -price_delta.min(0.0);
        let avg_loss =
            (self.prev_avg_loss * (N as f32 - 1.0) + loss) / N as f32;

        self.value = 100.0 - 100.0 / (1.0 + avg_gain / avg_loss);
        self.value
    }

    /// Replaces the most recent price used to calculate the RSI's value with
    /// `price`. This will update RSI's value to reflect the new prices.
    pub fn update(&mut self, price: f32) -> f32 {
        self.curr_price = price;
        self.calculate_value()
    }

    /// Updates the RSI value taking into consideration `prev_close_price` and
    /// `curr_price` as its most recent price change while still considering its
    /// previous price changes.
    pub fn add(&mut self, price: f32) -> f32 {
        let price_delta = self.curr_price - self.prev_price;

        let gain = price_delta.max(0.0);
        self.prev_avg_gain =
            (self.prev_avg_gain * (N as f32 - 1.0) + gain) / N as f32;

        let loss = -price_delta.min(0.0);
        self.prev_avg_loss =
            (self.prev_avg_loss * (N as f32 - 1.0) + loss) / N as f32;

        self.prev_price = self.curr_price;
        self.curr_price = price;

        self.calculate_value()
    }

    /// Returns the current RSI value.
    pub fn get_value(&self) -> f32 {
        self.value
    }
}

#[derive(Error, Debug)]
pub enum NewRsiError {
    #[error("Period is too small: Expected at least 1 but was {0}")]
    PeriodTooSmall(usize),
    #[error("Not enough prices: Expected at least {0} but received {1}")]
    NotEnoughClosePrices(usize, usize),
}

/// Moving average convergence divergence. `N` is the period for the fast EMA,
/// 'O' is the period for the slow EMA, and `P` is the period for the signal
/// line.
#[derive(Debug)]
pub struct Macd<const N: usize, const O: usize, const P: usize> {
    fast_ema: Ema<N>,
    slow_ema: Ema<O>,
    signal_line: Ema<P>,
    value: f32,
}

impl<const N: usize, const O: usize, const P: usize> Macd<N, O, P> {
    pub fn new(close_prices: &[f32]) -> Result<Self, NewMacdError> {
        if N >= O {
            return Err(NewMacdError::FastMovingEmaPeriodLargerThanOrEqualToSlowMovingEma(N, O));
        }

        if close_prices.len() < N {
            return Err(NewMacdError::NotEnoughClosePricesForFastMovingEma(
                close_prices.len(),
                N,
            ));
        }

        if close_prices.len() < O {
            return Err(NewMacdError::NotEnoughClosePricesForSlowMovingEma(
                close_prices.len(),
                O,
            ));
        }

        if close_prices.len() < O + P {
            return Err(NewMacdError::NotEnoughClosePricesForSignalLine(
                close_prices.len(),
                O + P,
            ));
        }

        // Both EMA's get `O` close prices so that they are in sync when calculating the signal line.
        let mut fast_ema = Ema::new(&close_prices[..O])
            .map_err(|e| NewMacdError::FailedToCreateEma(N, e))?;
        let mut slow_ema = Ema::new(&close_prices[..O])
            .map_err(|e| NewMacdError::FailedToCreateEma(O, e))?;

        // Add the next P close prices after the first O close prices so that we can the MACD's in order to calculate the signal line.
        let mut macds = [0.0; P];
        macds[0] = fast_ema.get_value() - slow_ema.get_value();
        for i in 1..P {
            let close_price = close_prices[O + i - 1];
            macds[i] = fast_ema.add(close_price) - slow_ema.add(close_price);
        }
        let mut signal_line = Ema::new(&macds)
            .map_err(|e| NewMacdError::FailedToCreateEma(P, e))?;

        // Add the remaining close prices to the ema's
        let mut macd = macds[P - 1];
        for i in O + P - 1..close_prices.len() {
            let close_price = close_prices[i];
            macd = fast_ema.add(close_price) - slow_ema.add(close_price);
            signal_line.add(macd);
        }

        Ok(Self {
            fast_ema,
            slow_ema,
            signal_line,
            value: macd,
        })
    }

    pub fn update(&mut self, value: f32) {
        self.value = self.fast_ema.update(value) - self.slow_ema.update(value);
        self.signal_line.update(self.value);
    }

    pub fn add(&mut self, value: f32) {
        self.value = self.fast_ema.add(value) - self.slow_ema.add(value);
        self.signal_line.add(self.value);
    }

    pub fn get_signal_line(&self) -> f32 {
        self.signal_line.get_value()
    }

    pub fn get_value(&self) -> f32 {
        self.value
    }
}

#[derive(Error, Debug)]
pub enum NewMacdError {
    #[error("Not enough close prices to create fast moving EMA: Expected {0} but received {1}")]
    NotEnoughClosePricesForFastMovingEma(usize, usize),
    #[error("Not enough close prices to create slow moving EMA: Expected {0} but received {1}")]
    NotEnoughClosePricesForSlowMovingEma(usize, usize),
    #[error(
    "Not enough close prices for signal line: Expected {0} but received {1}"
  )]
    NotEnoughClosePricesForSignalLine(usize, usize),
    #[error("The period for the fast moving EMA, {0}, is larger than or equal to the period for the slow moving EMA, {1}")]
    FastMovingEmaPeriodLargerThanOrEqualToSlowMovingEma(usize, usize),
    #[error("Failed to create EMA with period of {0}: {1}")]
    FailedToCreateEma(usize, NewEmaError),
}

/// Estimated moving average.
#[derive(Debug)]
pub struct Ema<const N: usize> {
    value: f32,
    prev_ema: f32,
    smoothing_factor: f32,
    one_minus_smoothing_factor: f32,
}

/// N is the period
impl<const N: usize> Ema<N> {
    pub fn new(data: &[f32]) -> Result<Self, NewEmaError> {
        if N < 2 {
            return Err(NewEmaError::PeriodTooSmall(N));
        }
        if data.len() < N {
            return Err(NewEmaError::NotEnoughValues(data.len(), N));
        }

        let smoothing_factor = 2.0 / (N as f32 + 1.0);
        let one_minus_smoothing_factor = 1.0 - smoothing_factor;

        // Get the initial EMA which is the average of the first N close prices
        let mut ema = 0.0;
        for i in 0..N {
            ema += data[i];
        }
        ema /= N as f32;

        // Calculate EMA for the remaining data
        let mut prev_ema = ema;
        for i in 1..data.len() {
            ema = data[i] * smoothing_factor + ema * one_minus_smoothing_factor;
            if i == data.len() - 2 {
                prev_ema = ema;
            }
        }

        Ok(Self {
            value: ema,
            prev_ema,
            smoothing_factor,
            one_minus_smoothing_factor,
        })
    }

    pub fn add(&mut self, value: f32) -> f32 {
        self.prev_ema = self.value;
        self.update(value)
    }

    pub fn update(&mut self, value: f32) -> f32 {
        self.value = value * self.smoothing_factor
            + self.prev_ema * self.one_minus_smoothing_factor;
        self.value
    }

    pub fn get_value(&self) -> f32 {
        self.value
    }
}

#[derive(Error, Debug)]
pub enum NewEmaError {
    #[error("Period is too small: Expected at least 2 but is {0}")]
    PeriodTooSmall(usize),
    #[error("Not enough data: Expected at least {0} but received {1}")]
    NotEnoughValues(usize, usize),
}

#[cfg(test)]
mod tests {
    use super::{Ema, Rsi, Sma, StdDev};

    fn assert_f32(actual: f32, expected: f32) {
        assert!((actual - expected).abs() < f32::EPSILON);
    }

    #[test]
    fn test_rsi_new() {
        const CLOSE_PRICES: [f32; 33] = [
            44.34, 44.09, 44.15, 43.61, 44.33, 44.83, 45.1, 45.42, 45.84,
            46.08, 45.89, 46.03, 45.61, 46.28, 46.28, 46., 46.03, 46.41, 46.22,
            45.64, 46.21, 46.25, 45.71, 46.45, 45.78, 45.35, 44.03, 44.18,
            44.22, 44.57, 43.42, 42.66, 43.13,
        ];
        const EXPECTED_RSI: f32 = 37.78879;

        let rsi = match Rsi::<14>::new(&CLOSE_PRICES) {
            Ok(rsi) => rsi,
            Err(e) => {
                panic!("Failed to create RSI: {}", e);
            }
        };
        assert_f32(rsi.get_value(), EXPECTED_RSI);
    }

    #[test]
    fn test_rsi_add() {
        const CLOSE_PRICES: [f32; 33] = [
            44.34, 44.09, 44.15, 43.61, 44.33, 44.83, 45.1, 45.42, 45.84,
            46.08, 45.89, 46.03, 45.61, 46.28, 46.28, 46., 46.03, 46.41, 46.22,
            45.64, 46.21, 46.25, 45.71, 46.45, 45.78, 45.35, 44.03, 44.18,
            44.22, 44.57, 43.42, 42.66, 43.13,
        ];
        const EXPECTED_RSI: f32 = 37.78879;

        let mut rsi = match Rsi::<14>::new(&CLOSE_PRICES[..32]) {
            Ok(rsi) => rsi,
            Err(e) => {
                panic!("Failed to create RSI: {}", e);
            }
        };

        rsi.add(CLOSE_PRICES[32]);
        assert_f32(rsi.get_value(), EXPECTED_RSI);
    }

    #[test]
    fn test_rsi_update() {
        const CLOSE_PRICES: [f32; 33] = [
            44.34, 44.09, 44.15, 43.61, 44.33, 44.83, 45.1, 45.42, 45.84,
            46.08, 45.89, 46.03, 45.61, 46.28, 46.28, 46., 46.03, 46.41, 46.22,
            45.64, 46.21, 46.25, 45.71, 46.45, 45.78, 45.35, 44.03, 44.18,
            44.22, 44.57, 43.42, 42.66, 12.0,
        ];
        const EXPECTED_RSI: f32 = 37.78879;

        let mut rsi = match Rsi::<14>::new(&CLOSE_PRICES) {
            Ok(rsi) => rsi,
            Err(e) => {
                panic!("Failed to create RSI: {}", e);
            }
        };

        // Then update it again so that it has the expected value.
        rsi.update(43.13);
        assert_f32(rsi.get_value(), EXPECTED_RSI);
    }

    #[test]
    fn test_ema_new() {
        const VALUES: [f32; 30] = [
            22.27, 22.19, 22.08, 22.17, 22.18, 22.13, 22.23, 22.43, 22.24,
            22.29, 22.15, 22.39, 22.38, 22.61, 23.36, 24.05, 23.75, 23.83,
            23.95, 23.63, 23.82, 23.87, 23.65, 23.19, 23.1, 23.33, 22.68, 23.1,
            22.4, 22.17,
        ];
        const EXPECTED_EMA: f32 = 22.91539;

        let ema = match Ema::<10>::new(&VALUES) {
            Ok(ema) => ema,
            Err(e) => {
                panic!("Failed to create EMA: {}", e);
            }
        };
        assert_f32(ema.get_value(), EXPECTED_EMA);
    }

    #[test]
    fn test_ema_add() {
        const VALUES: [f32; 30] = [
            22.27, 22.19, 22.08, 22.17, 22.18, 22.13, 22.23, 22.43, 22.24,
            22.29, 22.15, 22.39, 22.38, 22.61, 23.36, 24.05, 23.75, 23.83,
            23.95, 23.63, 23.82, 23.87, 23.65, 23.19, 23.1, 23.33, 22.68, 23.1,
            22.4, 22.17,
        ];
        const EXPECTED_EMA: f32 = 22.91539;

        let mut ema = match Ema::<10>::new(&VALUES[..29]) {
            Ok(ema) => ema,
            Err(e) => {
                panic!("Failed to create EMA: {}", e);
            }
        };
        ema.add(VALUES[29]);
        assert_f32(ema.get_value(), EXPECTED_EMA);
    }

    #[test]
    fn test_ema_update() {
        const VALUES: [f32; 30] = [
            22.27, 22.19, 22.08, 22.17, 22.18, 22.13, 22.23, 22.43, 22.24,
            22.29, 22.15, 22.39, 22.38, 22.61, 23.36, 24.05, 23.75, 23.83,
            23.95, 23.63, 23.82, 23.87, 23.65, 23.19, 23.1, 23.33, 22.68, 23.1,
            22.4, 0.0,
        ];
        const EXPECTED_EMA: f32 = 22.91539;

        let mut ema = match Ema::<10>::new(&VALUES) {
            Ok(ema) => ema,
            Err(e) => {
                panic!("Failed to create EMA: {}", e);
            }
        };
        ema.update(22.17);
        assert_f32(ema.get_value(), EXPECTED_EMA);
    }

    #[test]
    fn test_sma_new() {
        const VALUES: [f32; 9] = [
            22.27, 22.19, 22.08, 22.17, 22.18, 22.13, 22.23, 22.43, 22.24,
        ];
        const EXPECTED_SMA: f32 = 22.2575;

        let sma = match Sma::<4>::new(&VALUES) {
            Ok(sma) => sma,
            Err(e) => {
                panic!("Failed to create SMA: {}", e);
            }
        };
        assert_f32(sma.get_value(), EXPECTED_SMA);
    }

    #[test]
    fn test_sma_add() {
        const VALUES: [f32; 9] = [
            22.27, 22.19, 22.08, 22.17, 22.18, 22.13, 22.23, 22.43, 22.24,
        ];
        const EXPECTED_SMA: f32 = 22.2575;

        let mut sma = match Sma::<4>::new(&VALUES[..8]) {
            Ok(sma) => sma,
            Err(e) => {
                panic!("Failed to create SMA: {}", e);
            }
        };
        sma.add(VALUES[8]);
        assert_f32(sma.get_value(), EXPECTED_SMA);
    }

    #[test]
    fn test_sma_update() {
        const VALUES: [f32; 9] =
            [22.27, 22.19, 22.08, 22.17, 22.18, 22.13, 22.23, 22.43, 0.0];
        const EXPECTED_SMA: f32 = 22.2575;

        let mut sma = match Sma::<4>::new(&VALUES) {
            Ok(sma) => sma,
            Err(e) => {
                panic!("Failed to create SMA: {}", e);
            }
        };
        sma.update(22.24);
        assert_f32(sma.get_value(), EXPECTED_SMA);
    }

    #[test]
    fn test_std_dev_new() {
        const VALUES: [f32; 9] =
            [12.0, 10.0, 12.0, 23.0, 23.0, 16.0, 23.0, 21.0, 16.0];
        const EXPECT_STD_DEV: f32 = 5.2372293656638;

        let std_dev = match StdDev::<8>::new(&VALUES) {
            Ok(std_dev) => std_dev,
            Err(e) => {
                panic!("Failed to create standard deviation: {}", e);
            }
        };
        assert_f32(std_dev.get_value(), EXPECT_STD_DEV);
    }

    #[test]
    fn test_std_dev_add() {
        const VALUES: [f32; 9] =
            [12.0, 10.0, 12.0, 23.0, 23.0, 16.0, 23.0, 21.0, 16.0];
        const EXPECT_STD_DEV: f32 = 5.2372293656638;

        let mut std_dev = match StdDev::<8>::new(&VALUES[..8]) {
            Ok(std_dev) => std_dev,
            Err(e) => {
                panic!("Failed to create standard deviation: {}", e);
            }
        };
        std_dev.add(VALUES[8]);
        assert_f32(std_dev.get_value(), EXPECT_STD_DEV);
    }

    #[test]
    fn test_std_dev_update() {
        const VALUES: [f32; 9] =
            [12.0, 10.0, 12.0, 23.0, 23.0, 16.0, 23.0, 21.0, 0.0];
        const EXPECT_STD_DEV: f32 = 5.2372293656638;

        let mut std_dev = match StdDev::<8>::new(&VALUES[..9]) {
            Ok(std_dev) => std_dev,
            Err(e) => {
                panic!("Failed to create standard deviation: {}", e);
            }
        };
        std_dev.update(16.0);
        assert_f32(std_dev.get_value(), EXPECT_STD_DEV);
    }
}
