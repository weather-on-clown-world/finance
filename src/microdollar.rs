use {
    bytes::BytesMut,
    serde::{
        de::{Deserializer, Visitor},
        Deserialize, Serialize, Serializer,
    },
    std::{
        convert::{From, Into, TryFrom},
        fmt::{self, Display},
        iter::Sum,
        ops,
    },
    thiserror::Error,
    tokio_postgres::types::{FromSql, IsNull, ToSql, Type},
};

const MAX_MICRODOLLAR_DECIMAL_PLACES: usize = 6;

/// Represents an amount of currency. The amount is stored internally as
/// integer and multipled by 1000000 to avoid floating point arithmetic issues.
/// 1 cent == 10000 microdollars
/// 1 dollar == 1000000 microdollars
#[derive(Default, Copy, Clone, Debug, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Microdollar {
    /// The currency's value in dollars multipled by 1000000
    value: i64,
}

impl Microdollar {
    pub const fn new(value: i64) -> Self {
        Self { value }
    }

    pub fn abs(&self) -> Self {
        Self {
            value: self.value.abs(),
        }
    }

    pub fn lerp(&self, other: &Self, t: f64) -> Self {
        let t = t.clamp(0.0, 1.0);
        Self {
            value: self.value + (((other.value as f64 - self.value as f64) * t) as i64),
        }
    }

    pub fn is_zero(&self) -> bool {
        self.value == 0
    }

    pub fn is_negative(&self) -> bool {
        self.value < 0
    }

    pub fn truncate_to_cents(&self) -> Self {
        let value = self.value / 10000 * 10000;
        Self { value }
    }

    pub fn round_to_nearest_5_cents(&self) -> Self {
        let mut value = self.value / 10000;
        value += match value % 5 {
            4 => 1,
            3 => 2,
            2 => -2,
            1 => -1,
            -1 => 1,
            -2 => 2,
            -3 => -2,
            -4 => -1,
            _ => 0,
        };
        value *= 10000;
        Self { value }
    }

    pub fn value(&self) -> i64 {
        self.value
    }
}

impl Sum for Microdollar {
    fn sum<I>(iter: I) -> Self
    where
        I: Iterator<Item = Self>,
    {
        iter.fold(Microdollar { value: 0 }, |a, b| a + b)
    }
}

impl ToSql for Microdollar {
    fn to_sql(
        &self,
        ty: &Type,
        out: &mut BytesMut,
    ) -> Result<IsNull, Box<dyn std::error::Error + 'static + Send + Sync>> {
        self.value.to_sql(ty, out)
    }

    fn accepts(ty: &Type) -> bool {
        <i64 as ToSql>::accepts(ty)
    }

    tokio_postgres::types::to_sql_checked!();
}

impl<'a> FromSql<'a> for Microdollar {
    fn from_sql(
        ty: &Type,
        raw: &'a [u8],
    ) -> Result<Self, Box<dyn std::error::Error + Sync + Send>> {
        let value = <i64>::from_sql(ty, raw)?;
        Ok(Microdollar { value })
    }

    fn accepts(ty: &Type) -> bool {
        <i64 as FromSql>::accepts(ty)
    }
}

impl ops::Add<Microdollar> for Microdollar {
    type Output = Microdollar;

    fn add(self, rhs: Microdollar) -> Microdollar {
        Microdollar {
            value: self.value + rhs.value,
        }
    }
}

impl ops::AddAssign<Microdollar> for Microdollar {
    fn add_assign(&mut self, rhs: Microdollar) {
        self.value += rhs.value;
    }
}

impl ops::Sub<Microdollar> for Microdollar {
    type Output = Microdollar;

    fn sub(self, rhs: Microdollar) -> Microdollar {
        Microdollar {
            value: self.value - rhs.value,
        }
    }
}

impl ops::SubAssign<Microdollar> for Microdollar {
    fn sub_assign(&mut self, rhs: Microdollar) {
        self.value -= rhs.value;
    }
}

impl ops::Neg for Microdollar {
    type Output = Microdollar;

    fn neg(self) -> Microdollar {
        Microdollar { value: -self.value }
    }
}

// Do not implement `Mul` with `Microdollar` as the generic parameter because multiplying two currency values together doesn't make cents.
impl ops::Mul<i64> for Microdollar {
    type Output = Microdollar;

    fn mul(self, rhs: i64) -> Microdollar {
        Microdollar {
            value: self.value * rhs,
        }
    }
}

impl ops::Div<i64> for Microdollar {
    type Output = Microdollar;

    fn div(self, rhs: i64) -> Microdollar {
        Microdollar {
            value: self.value / rhs,
        }
    }
}

impl ops::Div<f32> for Microdollar {
    type Output = Microdollar;

    fn div(self, rhs: f32) -> Microdollar {
        Microdollar {
            value: self.value / rhs as i64,
        }
    }
}

impl ops::DivAssign<u32> for Microdollar {
    fn div_assign(&mut self, rhs: u32) {
        self.value /= rhs as i64;
    }
}

impl ops::Div<Microdollar> for Microdollar {
    type Output = f32;

    fn div(self, rhs: Microdollar) -> f32 {
        self.value as f32 / rhs.value as f32
    }
}

impl ops::Mul<u32> for Microdollar {
    type Output = Microdollar;

    fn mul(self, rhs: u32) -> Microdollar {
        Microdollar {
            value: self.value * rhs as i64,
        }
    }
}

impl ops::Mul<i32> for Microdollar {
    type Output = Microdollar;

    fn mul(self, rhs: i32) -> Microdollar {
        Microdollar {
            value: self.value * rhs as i64,
        }
    }
}

impl ops::Mul<f32> for Microdollar {
    type Output = Microdollar;

    fn mul(self, rhs: f32) -> Microdollar {
        Microdollar {
            value: (self.value as f32 * rhs) as i64,
        }
    }
}

impl Display for Microdollar {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let dollar = self.value / 1000000;
        let cents = (self.value % 1000000).abs();
        if f.alternate() {
            write!(f, "${}.{}", dollar, &format!("{:06}", cents)[..2])
        } else {
            write!(f, "{}.{:06}", dollar, cents)
        }
    }
}

/// Assumes that the unit of the provided value is dollars and not microdollars. (i.e. A variable with a value of 1200 represents $1200)
impl From<i32> for Microdollar {
    fn from(item: i32) -> Self {
        Self {
            value: ((item as i64) * 1000000),
        }
    }
}

/// Assumes that the unit of the provided value is dollars and not microdollars. (i.e. A variable with a value of 1200 represents $1200)
impl From<u32> for Microdollar {
    fn from(item: u32) -> Self {
        Self {
            value: ((item as i64) * 1000000),
        }
    }
}

/// Assumes that the unit of the provided value is dollars and not microdollars. (i.e. A variable with a value of 1200 represents $1200)
impl From<i64> for Microdollar {
    fn from(item: i64) -> Self {
        Self {
            value: item * 1000000,
        }
    }
}

/// Assumes that the unit of the provided value is dollars and not microdollars. (i.e. A variable with a value of 1200.00f represents $1200)
impl From<f32> for Microdollar {
    fn from(item: f32) -> Self {
        Self {
            value: (item * 1e6) as i64,
        }
    }
}

/// Assumes that the unit of the provided value is dollars and not microdollars. (i.e. A variable with a value of 1200.00 represents $1200)
impl From<f64> for Microdollar {
    fn from(item: f64) -> Self {
        Self {
            value: (item * 1e6) as i64,
        }
    }
}

// Unit of the returned value is dollars and not microdollars.
impl Into<f64> for &Microdollar {
    fn into(self) -> f64 {
        self.value as f64 / 1e6
    }
}

/// Assumes that the unit of the provided value is dollars and not microdollars. (i.e. A variable with a value of "1200" represents $1200)
impl TryFrom<&str> for Microdollar {
    type Error = MicrodollarTryFromError;

    fn try_from(item: &str) -> Result<Self, Self::Error> {
        let mut sum: i64 = 0;
        let mut sign = 1;
        let mut decimal = item.len();
        for (i, c) in item.chars().enumerate() {
            match c {
                '-' => sign = -1,
                '.' => {
                    if decimal == item.len() {
                        decimal = i + 1
                    } else {
                        return Err(Self::Error::MultipleDecimals);
                    }
                }
                c => match c.to_digit(10) {
                    Some(d) => {
                        sum = sum * 10;
                        sum = sum + d as i64;
                    }
                    None => return Err(Self::Error::IllegalChar(i)),
                },
            }
        }
        let decimal_count = item.len() - decimal;
        if decimal_count > MAX_MICRODOLLAR_DECIMAL_PLACES {
            return Err(Self::Error::TooManyDecimalPlaces(
                MAX_MICRODOLLAR_DECIMAL_PLACES,
                decimal_count,
            ));
        }
        let value = sum
            * sign
            * (10 as i64).pow(MAX_MICRODOLLAR_DECIMAL_PLACES as u32 - decimal_count as u32);
        Ok(Self { value })
    }
}

#[derive(Error, Debug, Eq, PartialEq)]
pub enum MicrodollarTryFromError {
    #[error("String contains multiple decimals")]
    MultipleDecimals,
    #[error("Character at position {0} is not digit or a decimal")]
    IllegalChar(usize),
    #[error("Too many decimal places (max is {0} but found {1}")]
    TooManyDecimalPlaces(usize, usize),
}

/// Assumes that the unit of the provided value is dollars and not microdollars. (i.e. A variable with a value of 1200 represents $1200)
impl<'de> Deserialize<'de> for Microdollar {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        struct MicrodollarVisitor;

        impl<'de> Visitor<'de> for MicrodollarVisitor {
            type Value = Microdollar;

            fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
                formatter.write_str("a floating point number")
            }

            fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Microdollar::try_from(v).map_err(|err| E::custom(format!("{}", err)))
            }

            fn visit_i32<E>(self, v: i32) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Microdollar::from(v))
            }

            fn visit_u32<E>(self, v: u32) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Microdollar::from(v))
            }

            fn visit_i64<E>(self, v: i64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Microdollar::from(v))
            }

            fn visit_f32<E>(self, v: f32) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Microdollar::from(v))
            }

            fn visit_f64<E>(self, v: f64) -> Result<Self::Value, E>
            where
                E: serde::de::Error,
            {
                Ok(Microdollar::from(v))
            }
        }

        deserializer.deserialize_any(MicrodollarVisitor)
    }
}

impl Serialize for Microdollar {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        serializer.serialize_f64(Into::<f64>::into(self))
    }
}

#[cfg(test)]
mod tests {
    use {super::*, std::convert::TryFrom};

    #[test]
    fn microdollar_ops() {
        // add
        assert_eq!(
            Microdollar::new(12349876) + Microdollar::new(0),
            Microdollar::new(12349876)
        );
        assert_eq!(
            Microdollar::new(1000000) + Microdollar::new(5000000),
            Microdollar::new(6000000)
        );
        assert_eq!(
            Microdollar::new(2000000) + Microdollar::new(-100000),
            Microdollar::new(1900000)
        );

        // sub
        assert_eq!(
            Microdollar::new(534545) - Microdollar::new(0),
            Microdollar::new(534545)
        );
        assert_eq!(
            Microdollar::new(1000000) - Microdollar::new(3000000),
            Microdollar::new(-2000000)
        );
        assert_eq!(
            Microdollar::new(1000000) - Microdollar::new(-30000000),
            Microdollar::new(31000000)
        );

        // add-assign
        let mut microdollar = Microdollar::new(524554);
        microdollar += Microdollar::new(0);
        assert_eq!(microdollar, Microdollar::new(524554));

        let mut microdollar = Microdollar::new(200000);
        microdollar += Microdollar::new(4000);
        assert_eq!(microdollar, Microdollar::new(204000));

        // sub-assign
        let mut microdollar = Microdollar::new(71234);
        microdollar -= Microdollar::new(0);
        assert_eq!(microdollar, Microdollar::new(71234));

        let mut microdollar = Microdollar::new(80000);
        microdollar -= Microdollar::new(3000);
        assert_eq!(microdollar, Microdollar::new(77000));

        // neg
        assert_eq!(-Microdollar::new(0), Microdollar::new(0));
        assert_eq!(-Microdollar::new(123456), Microdollar::new(-123456));
        assert_eq!(-Microdollar::new(-4567), Microdollar::new(4567));
    }

    #[test]
    fn str_to_microdollar() {
        let microdollar = Microdollar::new(1000000);
        assert_eq!(microdollar, Microdollar::try_from("1").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("1.0").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("1.0000").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("01.00").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("00001.00").unwrap());

        let microdollar = Microdollar::new(100000);
        assert_eq!(microdollar, Microdollar::try_from("0.1").unwrap());
        assert_eq!(microdollar, Microdollar::try_from(".1").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("0.10").unwrap());
        assert_eq!(microdollar, Microdollar::try_from(".10").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("0000.100").unwrap());

        let microdollar = Microdollar::new(-1000000);
        assert_eq!(microdollar, Microdollar::try_from("-1").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("-1.0").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("-1.0000").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("-01.00").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("-00001.00").unwrap());

        let microdollar = Microdollar::new(7000000);
        assert_eq!(microdollar, Microdollar::try_from("7").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("7.0").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("7.0000").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("07.00").unwrap());

        let microdollar = Microdollar::new(1230000);
        assert_eq!(microdollar, Microdollar::try_from("1.23").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("01.23").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("01.230").unwrap());

        let microdollar = Microdollar::new(123456789000);
        assert_eq!(microdollar, Microdollar::try_from("123456.789").unwrap());

        let microdollar = Microdollar::new(-123456789000);
        assert_eq!(microdollar, Microdollar::try_from("-123456.789").unwrap());

        let microdollar = Microdollar { value: 6030000 };
        assert_eq!(microdollar, Microdollar::try_from("6.03").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("6.030").unwrap());
        assert_eq!(microdollar, Microdollar::try_from("06.030").unwrap());
    }

    #[test]
    fn microdollar_to_f64() {
        let microdollar = Microdollar::new(123456789000);
        assert_eq!(Into::<f64>::into(&microdollar), 123456.789);
    }

    #[test]
    fn f64_to_microdollar() {
        let microdollar = Microdollar::new(123456789000);
        assert_eq!(Microdollar::from(123456.789), microdollar);
    }

    #[test]
    fn display_microdollar() {
        let microdollar = Microdollar::new(0);
        assert_eq!(format!("{}", microdollar), "0.000000");
        assert_eq!(format!("{:#}", microdollar), "$0.00");

        let microdollar = Microdollar::new(9999);
        assert_eq!(format!("{}", microdollar), "0.009999");
        assert_eq!(format!("{:#}", microdollar), "$0.00");

        let microdollar = Microdollar::new(40000);
        assert_eq!(format!("{}", microdollar), "0.040000");
        assert_eq!(format!("{:#}", microdollar), "$0.04");

        let microdollar = Microdollar::new(123456789000);
        assert_eq!(format!("{}", microdollar), "123456.789000");
        assert_eq!(format!("{:#}", microdollar), "$123456.78");

        let microdollar = Microdollar::new(-987654321000);
        assert_eq!(format!("{}", microdollar), "-987654.321000");
        assert_eq!(format!("{:#}", microdollar), "$-987654.32");
    }

    #[test]
    fn truncate_to_cents() {
        let microdollar = Microdollar::new(123456789123).truncate_to_cents();
        assert_eq!(microdollar, Microdollar::new(123456780000));

        let microdollar = Microdollar::new(-123456789123).truncate_to_cents();
        assert_eq!(microdollar, Microdollar::new(-123456780000));
    }

    #[test]
    fn round_to_nearest_5_cents() {
        let microdollar = Microdollar::new(0).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(0));

        let microdollar = Microdollar::new(10000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(0));

        let microdollar = Microdollar::new(-10000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(0));

        let microdollar = Microdollar::new(20000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(0));

        let microdollar = Microdollar::new(-20000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(0));

        let microdollar = Microdollar::new(30000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(50000));

        let microdollar = Microdollar::new(-30000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-50000));

        let microdollar = Microdollar::new(40000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(50000));

        let microdollar = Microdollar::new(-40000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-50000));

        let microdollar = Microdollar::new(50000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(50000));

        let microdollar = Microdollar::new(-50000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-50000));

        let microdollar = Microdollar::new(60000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(50000));

        let microdollar = Microdollar::new(-60000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-50000));

        let microdollar = Microdollar::new(70000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(50000));

        let microdollar = Microdollar::new(-70000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-50000));

        let microdollar = Microdollar::new(80000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(100000));

        let microdollar = Microdollar::new(-80000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-100000));

        let microdollar = Microdollar::new(90000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(100000));

        let microdollar = Microdollar::new(-90000).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-100000));

        let microdollar = Microdollar::new(123456789123).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(123456800000));

        let microdollar = Microdollar::new(-123456789123).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-123456800000));

        let microdollar = Microdollar::new(123456769123).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(123456750000));

        let microdollar = Microdollar::new(-123456769123).round_to_nearest_5_cents();
        assert_eq!(microdollar, Microdollar::new(-123456750000));
    }
}
